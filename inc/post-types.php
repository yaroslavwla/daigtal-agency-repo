<?php
function register_post_types() {
    $labels = array(
        'name'               => 'Team',
        'singular_name'      => 'Team',
        'add_new'            => 'Add New Member',
        'add_new_item'       => 'Add New Member',
        'edit_item'          => 'Edit Member',
        'new_item'           => 'New Member',
        'all_items'          => 'All Members',
        'view_item'          => 'View Members',
        'search_items'       => 'Search Members',
        'not_found'          => 'No Members found',
        'not_found_in_trash' => 'No Members found in Trash',
        'menu_name'          => 'Team'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'team' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );
    register_post_type( 'team', $args );
}
add_action( 'init', 'register_post_types' );