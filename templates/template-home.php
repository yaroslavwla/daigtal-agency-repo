<?php
/**
 * Template Name: Home Page
 */
$heading_intro = get_field('heading_intro');
$image_intro = get_field('image_intro');
$content_intro = get_field('content_intro');
$button_intro = get_field('button_intro');
get_header(); ?>

    <!-- BEGIN of main content -->
    <main class="home-page">
        <?php if ($heading_intro || $image_intro || $content_intro || $button_intro) {
            ; ?>
            <section class="intro">
                <div class="intro__lines show-for-large"><span></span>
                    <div></div>
                </div>
                <div class="grid-container">
                    <div class="grid-x grid-margin-x align-middle">
                        <div class="cell medium-6">
                            <?php if ($heading_intro) {
                                ; ?>
                                <h1 class="h2 intro__heading"><?php echo $heading_intro; ?></h1>
                            <?php }; ?>
                            <?php if ($content_intro) {
                                ; ?>
                                <p class="intro__content"><?php echo $content_intro; ?></p>
                            <?php }; ?>
                            <?php if ($button_intro):
                                $link_url = $button_intro['url'];
                                $link_title = $button_intro['title'];
                                $link_target = $button_intro['target'] ? $button_intro['target'] : '_self'; ?>
                                <a class="button intro__button"
                                   href="<?php echo esc_url($link_url); ?>"
                                   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                        <div class="cell medium-6 text-center">
                            <?php if ($image_intro) {
                                ; ?>
                                <?php display_svg($image_intro, 'intro__image'); ?>
                            <?php }; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php }; ?>

        <?php if (have_rows('linkedin_content')): ?>
            <?php $counter = 0; ?>
            <section class="linkedin">
                <div class="grid-container">
                    <?php while (have_rows('linkedin_content')): the_row();
                        $image = get_sub_field('image');
                        $title = get_sub_field('title');
                        $text = get_sub_field('text');
                        $link = get_sub_field('button');
                        $counter++; ?>
                        <div class="linkedin__wrapper">
                            <div class="linkedin__left">
                                <div class="linkedin__left-wrapper">
                                    <div class="linkedin__left-counter"><?php echo str_pad($counter, 2, '0', STR_PAD_LEFT); ?></div>
                                    <?php if (!empty($image)): ?>
                                        <div class="linkedin__left-image">
                                            <img src="<?php echo esc_url($image['url']); ?>"
                                                 alt="<?php echo esc_attr($image['title']); ?>"/>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="linkedin__right">
                                <h3 class="linkedin__right-title"><?php echo $title; ?></h3>
                                <div class="linkedin__right-text"><?php echo $text; ?></div>
                                <?php if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class="button linkedin__right-button" href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>

        <?php
        $team_title   = get_field( 'team_title' );
        $team_members = get_field( 'team_members' );
        if ( $team_title || $team_members ): ?>
            <section class="team">
                <div class="intro__lines show-for-large"><span></span>
                    <div></div>
                </div>
                <div class="grid-container">
                    <?php if ( $team_title = get_field( 'team_title' ) ): ?>
                        <h2 class="team__heading"><?php echo $team_title; ?></h2>
                    <?php endif; ?>
                    <?php $team_members = get_field( 'team_members' ); ?>
                    <?php if ( $team_members ): ?>
                        <div class="team__inner">
                            <?php foreach ( $team_members as $team_member ):
                                $image = get_the_post_thumbnail( $team_member->ID );
                                $title = get_the_title( $team_member->ID );
                                $team_position = get_field( 'team_position', $team_member->ID );
                                $text = get_the_content( '', '', $team_member->ID );
                                $team_quote = get_field( 'team_quote', $team_member->ID );
                                ?>
                                <div class="team__member">
                                    <div class="team__photo"><?php echo $image; ?></div>
                                    <h3 class="team__title"><?php echo $title; ?></h3>
                                    <p class="team__position"><?php echo $team_position; ?></p>
                                    <div class="team__text"><?php echo wpautop($text); ?></div>
                                    <p class="team__quote"><?php echo $team_quote; ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        <?php endif; ?>

    </main>
    <!-- END of main content -->

<?php get_footer(); ?>