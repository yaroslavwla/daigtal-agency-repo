<?php
/**
 * Footer
 */
$heading_footer = get_field('heading_footer', 'options');
$form_footer = get_field('form_footer', 'options');
$contacts_footer = get_field('contacts_footer', 'options');
$copyright = get_field('copyright', 'options');
?>

<!-- BEGIN of footer -->
<footer class="footer">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell medium-6">
                <div class="footer__left">
                    <div class="footer__contact-wrap">
                        <?php if ($heading_footer) {
                            ; ?>
                            <h2 class="footer__heading"><?php echo $heading_footer; ?></h2>
                        <?php }; ?>
                        <?php if ($contacts_footer) {
                            ; ?>
                            <div class="footer__contacts"><?php echo $contacts_footer; ?></div>
                        <?php }; ?>
                        <?php if (have_rows('socials_footer', 'options')): ?>
                            <div class="footer__socials">
                                <?php while (have_rows('socials_footer', 'options')) : the_row(); ?>
                                    <?php $icon = get_sub_field('icon');
                                    $link = get_sub_field('link');
                                    $text = get_sub_field('text'); ?>
                                    <a class="footer__social"
                                       href="<?php echo $link; ?>"><?php echo wp_get_attachment_image($icon['ID'], 'medium', false, array('class' => '')); ?>
                                        <p><?php echo $text; ?></p></a>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?php if ($copyright) {
                        ; ?>
                        <div class="footer__copyright">
                            <?php echo $copyright; ?>
                        </div>
                    <?php }; ?>
                </div>
            </div>
            <div class="cell medium-6">
                <?php if ($form_footer) {
                    ; ?>
                    <div class="footer__form">
                        <?php echo do_shortcode('[gravityform id="' . $form_footer['id'] . '" title="false" description="false" ajax="true"]'); ?>
                    </div>
                <?php }; ?>
            </div>
        </div>
    </div>
</footer>
<!-- END of footer -->

<?php wp_footer(); ?>

</body>
</html>
